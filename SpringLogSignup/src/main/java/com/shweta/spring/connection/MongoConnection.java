package com.shweta.spring.connection;


import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.stereotype.Repository;
import com.shweta.spring.models.*;
import com.mongodb.MongoClient;

@Repository
public class MongoConnection {		

		private static final MongoConnection INSTANCE = new MongoConnection();
		private static final String DB_NAME = "information";

		private Morphia morphia = null;
		private Datastore datastore = null;
		private MongoClient mongoClient = null;

		private MongoConnection() {
			morphia = new Morphia();
			
			mongoClient = new MongoClient("localhost",27017);
			
			if (mongoClient != null) {
				datastore = morphia.createDatastore(mongoClient, DB_NAME);
				morphia.map(User.class);
			} else {
				mongoClient = new MongoClient("localhost", 27017);
				datastore = morphia.createDatastore(mongoClient, DB_NAME);
				morphia.map(User.class);
			}			
		}
		
		public static MongoConnection getInstance() {
			
			return INSTANCE;
		}

		public Morphia getMorphia() {
			return morphia;
		}

		public void setMorphia(Morphia morphia) {
			this.morphia = morphia;
		}

		public Datastore getDatastore() {
			return datastore;
		}

		public void setDatastore(Datastore datastore) {
			this.datastore = datastore;
		}

		public MongoClient getMongoClient() {
			return mongoClient;
		}

		public void setMongoClient(MongoClient mongoClient) {
			this.mongoClient = mongoClient;
		}

	
}