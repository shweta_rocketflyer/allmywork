package com.shweta.spring.dao;

import java.util.List;

import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import com.shweta.spring.connection.MongoConnection;
import com.shweta.spring.models.Login;
import com.shweta.spring.models.User;

@Repository
public class LoginDao {
	
	public String checkUser(Login login)
    {
		String uname = login.getUsername();
        String pass =  login.getPassword();
        
        
        
        MongoConnection con= MongoConnection.getInstance();
        
        
        Query<User> q =con.getDatastore().createQuery(User.class)
                .filter("username ", uname)
                .filter("password", pass);        
        
//		        q.and(
//		        	    q.criteria("username").equal(uname),
//		        	    q.criteria("password").equal(pass)
//		        	);

        List<User> u=q.asList();
        
        if(!u.isEmpty())
        {
           	return "Success";
         }
        
        else
        {
        	return "Invalid Input";
        }      
        
    }
}
