package com.shweta.spring.dao;

import java.util.*;

import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;
import com.shweta.spring.connection.MongoConnection;
import com.shweta.spring.models.User;

@Repository
public class RegisterDao {

	
	public String registerUser(User user)
    {
		int i=0;
       
        String uname = user.getUsername();       
        
        MongoConnection con= MongoConnection.getInstance();       
        
        Query<User> q =con.getDatastore().createQuery(User.class)
                .filter("username ", uname);
        
        List<User> u=q.asList();
        System.out.println("userdata"+u);
        
        if(u.isEmpty())
        {
        	con.getDatastore().save(user);
            i++;
            
            if(i>0)
            {
            	return "Success";
            }
            else
            {
            	return "Something went wrong";
            }
        }
        else
        {
        	return "Already existed";
        }     
        
    }       

}
