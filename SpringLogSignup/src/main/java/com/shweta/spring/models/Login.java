package com.shweta.spring.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;
import org.springframework.stereotype.Repository;

@Entity(value="Login_info")
@Repository
public class Login {
	
	@Id private ObjectId id;		
	@Property private String password;
	@Property private String username;
	
	
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Login(String password, String username) {
		super();
		this.password = password;
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	

}
