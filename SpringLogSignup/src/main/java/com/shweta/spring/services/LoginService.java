package com.shweta.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shweta.spring.dao.LoginDao;
import com.shweta.spring.models.Login;


@Service
public class LoginService {
	
	@Autowired
	private LoginDao logDao;
	public String createUser(Login login) {
		return logDao.checkUser(login);
}

}
