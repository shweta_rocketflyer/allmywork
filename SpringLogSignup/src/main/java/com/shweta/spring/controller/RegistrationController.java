package com.shweta.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shweta.spring.models.User;
import com.shweta.spring.services.UserService;

@Controller
public class RegistrationController {
	
	@Autowired
	private UserService userService;	
	
	@RequestMapping(value="/register", method=RequestMethod.GET)
	public ModelAndView showForm() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("register");
		return mv;
	}

	@RequestMapping(value="/processform", method = RequestMethod.POST)
	public ModelAndView handleForm(@ModelAttribute("user") User user,@RequestParam("firstname")String fname,@RequestParam("lastname")String lname,@RequestParam("password")String pass,@RequestParam("email")String email,@RequestParam("username")String uname) {
		
		user.setFirstname(fname);
		user.setLastname(lname);
		user.setEmail(email);
		user.setUsername(uname);
		user.setPassword(pass);
		
		ModelAndView mv=new ModelAndView();
		mv.addObject("users",user);
		
		if (user.getFirstname().equals("") || user.getLastname().equals("")||user.getEmail().equals("")||user.getUsername().equals("")) {
			mv.setViewName("register");
			return mv;
		}		
		
		String message = userService.createUser(user);
		System.out.println(message);
		if(message.equals("success"))   //On success, you can display a message to user on Home page
        {
			mv.addObject("message",". Please login!!");
			mv.setViewName("login");
			return mv;
        }
       
		else
		{
			mv.addObject("message",message+". Please Login !!");
			mv.setViewName("login");
			return mv;
		}
	   
	}

}
	
	


