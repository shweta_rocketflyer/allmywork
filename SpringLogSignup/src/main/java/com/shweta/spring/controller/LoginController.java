package com.shweta.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.shweta.spring.models.Login;
import com.shweta.spring.services.LoginService;

@Controller
public class LoginController {
	
	@Autowired
	private LoginService logService;	
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView showForm() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("login");
		return mv;
	}
	
	
	@RequestMapping(value="/loginprocess", method = RequestMethod.POST)
	public ModelAndView handleloginForm(@ModelAttribute Login login,@RequestParam("password")String pass,@RequestParam("username")String uname) {
		
		login.setUsername(uname);
		login.setPassword(pass);
		
		ModelAndView mv=new ModelAndView();
		mv.addObject("user",login);
		
		if (uname.equals("")||pass.equals("")) {
			mv.setViewName("login");
			return mv;
		}	
		
		String message = logService.createUser(login);
		if(message.equalsIgnoreCase("success"))
		{
			mv.setViewName("welcome");
			return mv;
		}
		
		else
		{
			mv.setViewName("login");
			return mv;
			
		}
	   
	}

}
