package com.shweta.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shweta.spring.models.User;
import com.shweta.spring.services.UserService;

@Controller
public class RegistrationController {
	
	@Autowired
	private UserService userService;	
	
	@RequestMapping("/register")
	public String showForm(Model m) {
		return "registeration";
	}

	@RequestMapping(path = "/processform", method = RequestMethod.POST)
	public String handleForm(@ModelAttribute("user") User user, Model model) {
		
		if (user.getFirstname().equals("") || user.getLastname().equals("")||user.getEmail().equals("")||user.getUsername().equals("")) {
			return "redirect:/register";
		}
		
		String message = userService.createUser(user);
		System.out.println(message);

		if(message.equalsIgnoreCase("SUCCESS"))   //On success, you can display a message to user on Home page
        {
			return "welcome";
        }
       
		else
		{
			return "redirect:/register";
		}
	   
	}

}
	
	


