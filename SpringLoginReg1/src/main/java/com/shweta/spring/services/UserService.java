package com.shweta.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shweta.spring.dao.RegisterDao;
import com.shweta.spring.models.User;


@Service
public class UserService {
	
		@Autowired
		private RegisterDao regDao;
		public String createUser(User user) {
			return regDao.registerUser(user);
	}
}
